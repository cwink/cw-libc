#ifndef CW_H
#define CW_H

typedef char CW_S8;

typedef unsigned char CW_U8;

typedef short CW_S16;

typedef unsigned short CW_U16;

typedef int CW_S32;

typedef unsigned int CW_U32;

typedef long CW_S64;

typedef unsigned long CW_U64;

typedef enum
{
  CW_BOOL_FALSE = 0,
  CW_BOOL_TRUE = 1,
  CW_BOOL_MAX = 2
} CW_BOOL;

#endif /* CW_H */
