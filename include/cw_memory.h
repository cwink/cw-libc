#ifndef CW_MEMORY_H
#define CW_MEMORY_H

#include "cw.h"

#include <stdlib.h>

#define CW_MEMORY_CREATE(VARIABLE, TYPE, SIZE)                                \
  cw_memory_create ((void **)(VARIABLE), sizeof (TYPE) * (SIZE))

#define CW_MEMORY_RECREATE(VARIABLE, TYPE, SIZE)                              \
  cw_memory_recreate ((void **)(VARIABLE), sizeof (TYPE) * (SIZE))

#define CW_MEMORY_DESTROY(VARIABLE) cw_memory_destroy ((void **)(VARIABLE))

CW_BOOL cw_memory_create (void **mem, size_t size);

CW_BOOL cw_memory_recreate (void **mem, size_t size);

CW_BOOL cw_memory_destroy (void **mem);

#endif /* CW_MEMORY_H */
