#ifndef CW_ERROR_H
#define CW_ERROR_H

#include "cw.h"

#include <stdlib.h>

#ifndef CW_ERROR_DISABLE
#define CW_ERROR_CALL(LEVEL, MESSAGE)                                         \
  cw_error_callback_get () (LEVEL, __FILE__, __LINE__, MESSAGE)
#else
#define CW_ERROR_CALL(LEVEL, MESSAGE)                                         \
  do                                                                          \
    {                                                                         \
    }                                                                         \
  while (0)
#endif

typedef enum
{
  CW_ERROR_LEVEL_CRITICAL = 0,
  CW_ERROR_LEVEL_ERROR = 1,
  CW_ERROR_LEVEL_WARNING = 2,
  CW_ERROR_LEVEL_MAX = 3
} CW_ERROR_LEVEL;

typedef void (*CW_ERROR_CALLBACK) (const CW_ERROR_LEVEL level,
                                   const char *const file, const size_t line,
                                   const char *const message);

CW_BOOL cw_error_callback_set (CW_ERROR_CALLBACK callback);

CW_ERROR_CALLBACK cw_error_callback_get (void);

#endif /* CW_ERROR_H */
