#ifndef CW_FILE_H
#define CW_FILE_H

#include "cw.h"

CW_BOOL cw_file_get_filename (char *destination, const char *source);

#endif /* CW_FILE_H */
