#ifndef CW_TEST_H
#define CW_TEST_H

#include "cw_time.h"

#include <stdio.h>

#define CW_TEST_MAIN(BODY)                                                    \
  int main (void)                                                             \
  {                                                                           \
    double main_start_ = 0.0;                                                 \
                                                                              \
    double main_stop_ = 0.0;                                                  \
                                                                              \
    fprintf (stdout, "=== %s\n", __FILE__);                                   \
                                                                              \
    cw_time_get_milliseconds (&main_start_);                                  \
                                                                              \
    BODY                                                                      \
                                                                              \
        cw_time_get_milliseconds (&main_stop_);                               \
                                                                              \
    fprintf (stdout, "main testing finished in %lf milliseconds.\n",          \
             main_stop_ - main_start_);                                       \
  }

#define CW_TEST_CREATE(FILE_NAME, FUNCTION_NAME, BODY)                        \
  static void cw_test_##FILE_NAME##_##FUNCTION_NAME (void)                    \
  {                                                                           \
    double function_start_ = 0.0;                                             \
                                                                              \
    double function_stop_ = 0.0;                                              \
                                                                              \
    fprintf (stdout, "  --- " #FUNCTION_NAME "\n");                           \
                                                                              \
    cw_time_get_milliseconds (&function_start_);                              \
                                                                              \
    BODY                                                                      \
                                                                              \
        cw_time_get_milliseconds (&function_stop_);                           \
                                                                              \
    fprintf (stdout, "  function testing finished in %lf milliseconds.\n",    \
             function_stop_ - function_start_);                               \
  }

#define CW_TEST_CALL(FILE_NAME, FUNCTION_NAME)                                \
  cw_test_##FILE_NAME##_##FUNCTION_NAME ()

#define CW_TEST_ASSERT(CONDITION)                                             \
  do                                                                          \
    {                                                                         \
      double start_ = 0.0;                                                    \
                                                                              \
      double stop_ = 0.0;                                                     \
                                                                              \
      CW_BOOL passed_ = CW_BOOL_FALSE;                                        \
                                                                              \
      cw_time_get_milliseconds (&start_);                                     \
                                                                              \
      passed_ = !(CONDITION) == 0 ? CW_BOOL_TRUE : CW_BOOL_FALSE;             \
                                                                              \
      cw_time_get_milliseconds (&stop_);                                      \
                                                                              \
      if (passed_ != CW_BOOL_TRUE)                                            \
        {                                                                     \
          fprintf (stderr,                                                    \
                   "    FAILED:%d: condition '" #CONDITION                    \
                   "' failed in %lf milliseconds\n",                          \
                   __LINE__, stop_ - start_);                                 \
        }                                                                     \
      else                                                                    \
        {                                                                     \
          fprintf (stdout,                                                    \
                   "    PASSED:%d: condition '" #CONDITION                    \
                   "' passed in %lf milliseconds\n",                          \
                   __LINE__, stop_ - start_);                                 \
        }                                                                     \
    }                                                                         \
  while (0)

#endif /* CW_TEST_H */
