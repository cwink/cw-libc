#ifndef CW_DATA_VECTOR_H
#define CW_DATA_VECTOR_H

#include "cw.h"
#include "cw_data_compare.h"
#include "cw_data_copy.h"
#include "cw_data_destroy.h"

#include <stdlib.h>

#define CW_DATA_VECTOR_CREATE(VECTOR, TYPE, LENGTH, COPY, COMPARE, DESTROY)   \
  cw_data_vector_create (VECTOR, sizeof (TYPE) * LENGTH, COPY, COMPARE,       \
                         DESTROY)

typedef struct cw_data_vector CW_DATA_VECTOR;

CW_BOOL cw_data_vector_create (CW_DATA_VECTOR **vector, size_t capacity,
                               CW_DATA_COPY copy, CW_DATA_COMPARE compare,
                               CW_DATA_DESTROY destroy);

CW_BOOL cw_data_vector_copy (CW_DATA_VECTOR **destination,
                             const CW_DATA_VECTOR *source);

CW_BOOL cw_data_vector_destroy (CW_DATA_VECTOR **vector);

CW_BOOL cw_data_vector_compare (CW_BOOL *result, const CW_DATA_VECTOR *first,
                                const CW_DATA_VECTOR *second);

CW_BOOL cw_data_vector_insert (CW_DATA_VECTOR *vector, void *value,
                               size_t index);

CW_BOOL cw_data_vector_erase (CW_DATA_VECTOR *vector, size_t index);

CW_BOOL cw_data_vector_length (const CW_DATA_VECTOR *vector, size_t *length);

CW_BOOL cw_data_vector_get (const CW_DATA_VECTOR *vector, size_t index,
                            void **value);

CW_BOOL cw_data_vector_set (const CW_DATA_VECTOR *vector, size_t index,
                            void *value);

#endif /* CW_DATA_VECTOR_H */
