#ifndef CW_DATA_COPY_H
#define CW_DATA_COPY_H

#include "cw.h"

typedef CW_BOOL (*CW_DATA_COPY) (void **, const void *const);

CW_BOOL cw_data_copy_c_string (void **result, const void *value);

CW_BOOL cw_data_copy_char (void **result, const void *value);

CW_BOOL cw_data_copy_s8 (void **result, const void *value);

CW_BOOL cw_data_copy_u8 (void **result, const void *value);

CW_BOOL cw_data_copy_s16 (void **result, const void *value);

CW_BOOL cw_data_copy_u16 (void **result, const void *value);

CW_BOOL cw_data_copy_s32 (void **result, const void *value);

CW_BOOL cw_data_copy_u32 (void **result, const void *value);

CW_BOOL cw_data_copy_s64 (void **result, const void *value);

CW_BOOL cw_data_copy_u64 (void **result, const void *value);

CW_BOOL cw_data_copy_size (void **result, const void *value);

CW_BOOL cw_data_copy_bool (void **result, const void *value);

CW_BOOL cw_data_copy_vector (void **result, const void *value);

#endif /* CW_DATA_COPY_H */
