#ifndef CW_DATA_COMPARE_H
#define CW_DATA_COMPARE_H

#include "cw.h"

typedef CW_BOOL (*CW_DATA_COMPARE) (CW_BOOL *const, const void *const,
                                    const void *const);

CW_BOOL cw_data_compare_c_string (CW_BOOL *result, const void *first,
                                  const void *second);

CW_BOOL cw_data_compare_char (CW_BOOL *result, const void *first,
                              const void *second);

CW_BOOL cw_data_compare_s8 (CW_BOOL *result, const void *first,
                            const void *second);

CW_BOOL cw_data_compare_u8 (CW_BOOL *result, const void *first,
                            const void *second);

CW_BOOL cw_data_compare_s16 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_u16 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_s32 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_u32 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_s64 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_u64 (CW_BOOL *result, const void *first,
                             const void *second);

CW_BOOL cw_data_compare_size (CW_BOOL *result, const void *first,
                              const void *second);

CW_BOOL cw_data_compare_bool (CW_BOOL *result, const void *first,
                              const void *second);

CW_BOOL cw_data_compare_vector (CW_BOOL *result, const void *first,
                                const void *second);

#endif /* CW_DATA_COMPARE_H */
