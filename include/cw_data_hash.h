#ifndef CW_DATA_HASH_H
#define CW_DATA_HASH_H

#include "cw.h"

#include <stdlib.h>

typedef CW_BOOL (*CW_DATA_HASH) (CW_U64 *const, const void *const);

CW_BOOL cw_data_hash_fnv1a (CW_U64 *result, const CW_U8 *data, size_t length);

CW_BOOL cw_data_hash_jenkins (CW_U32 *result, const CW_U8 *data,
                              size_t length);

CW_BOOL cw_data_hash_c_string (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_char (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_u8 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_s8 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_s16 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_u16 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_s32 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_u32 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_s64 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_u64 (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_size (CW_U64 *result, const void *data);

CW_BOOL cw_data_hash_bool (CW_U64 *result, const void *data);

#endif /* CW_DATA_HASH_H */
