#ifndef CW_OS_H
#define CW_OS_H

#undef CW_OS_WINDOWS

#undef CW_OS_LINUX

#undef CW_OS_UNIX

#undef CW_OS_APPLE

#undef CW_OS_ANDROID

#undef CW_OS_UNKNOWN

#if defined(WIN32) || defined(_WIN32) || defined(_WIN64)                      \
    || defined(__WIN32__) || defined(__NT__)
#define CW_OS_WINDOWS
#elif defined(__MACH__) || defined(__APPLE__)
#define CW_OS_APPLE
#elif __linux__
#define CW_OS_LINUX
#elif __unix__
#define CW_OS_UNIX
#elif __ANDROID__
#define CW_OS_ANDROID
#else
#define CW_OS_UKNOWN
#endif /* CW_OS */

#ifdef CW_OS_WINDOWS
#define CW_OS_PATH_DELIMITER '\\'
#else
#define CW_OS_PATH_DELIMITER '/'
#endif /* CW_OS_WINDOWS */

#endif /* CW_OS_H */
