#ifndef CW_TIME_H
#define CW_TIME_H

#include "cw.h"

#include <sys/time.h>

CW_BOOL cw_time_get_milliseconds (double *value);

#endif /* CW_TIME_H */
