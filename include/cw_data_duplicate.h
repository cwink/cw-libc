#ifndef CW_DATA_DUPLICATE_H
#define CW_DATA_DUPLICATE_H

#include "cw.h"
#include "cw_data_vector.h"

#include <stdlib.h>

char *cw_data_duplicate_c_string (const char *value);

char *cw_data_duplicate_char (char value);

CW_S8 *cw_data_duplicate_s8 (CW_S8 value);

CW_U8 *cw_data_duplicate_u8 (CW_U8 value);

CW_S16 *cw_data_duplicate_s16 (CW_S16 value);

CW_U16 *cw_data_duplicate_u16 (CW_U16 value);

CW_S32 *cw_data_duplicate_s32 (CW_S32 value);

CW_U32 *cw_data_duplicate_u32 (CW_U32 value);

CW_S64 *cw_data_duplicate_s64 (CW_S64 value);

CW_U64 *cw_data_duplicate_u64 (CW_U64 value);

size_t *cw_data_duplicate_size (size_t value);

CW_BOOL *cw_data_duplicate_bool (CW_BOOL value);

CW_DATA_VECTOR *cw_data_duplicate_vector (const CW_DATA_VECTOR *value);

#endif /* CW_DATA_DUPLICATE_H */
