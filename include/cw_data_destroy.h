#ifndef CW_DATA_DESTROY_H
#define CW_DATA_DESTROY_H

#include "cw.h"

typedef CW_BOOL (*CW_DATA_DESTROY) (void **);

CW_BOOL cw_data_destroy_c_string (void **value);

CW_BOOL cw_data_destroy_char (void **value);

CW_BOOL cw_data_destroy_s8 (void **value);

CW_BOOL cw_data_destroy_u8 (void **value);

CW_BOOL cw_data_destroy_s16 (void **value);

CW_BOOL cw_data_destroy_u16 (void **value);

CW_BOOL cw_data_destroy_s32 (void **value);

CW_BOOL cw_data_destroy_u32 (void **value);

CW_BOOL cw_data_destroy_s64 (void **value);

CW_BOOL cw_data_destroy_u64 (void **value);

CW_BOOL cw_data_destroy_size (void **value);

CW_BOOL cw_data_destroy_bool (void **value);

CW_BOOL cw_data_destroy_vector (void **value);

#endif /* CW_DATA_DESTROY_H */
