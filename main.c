#include "include/cw.h"
#include "include/cw_data_compare.h"
#include "include/cw_data_copy.h"
#include "include/cw_data_destroy.h"
#include "include/cw_data_duplicate.h"
#include "include/cw_data_hash.h"
#include "include/cw_data_vector.h"
#include "include/cw_error.h"
#include "include/cw_memory.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (void)
{
  size_t index = 0;

  size_t length = 0;

  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_DATA_VECTOR *g = NULL;

  CW_S32 *c = cw_data_duplicate_s32 (13);

  CW_S32 *d = cw_data_duplicate_s32 (102);

  CW_S32 *e = cw_data_duplicate_s32 (-87);

  CW_S32 *f = cw_data_duplicate_s32 (-9);

  CW_BOOL equal = CW_BOOL_TRUE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&g, char, 2, cw_data_copy_char, cw_data_compare_char,
                         cw_data_destroy_char);

  cw_data_vector_insert (g, cw_data_duplicate_char('T'), 0);

  cw_data_vector_insert (g, cw_data_duplicate_char('A'), 1);

  cw_data_vector_insert (a, c, 0);

  cw_data_vector_insert (a, d, 1);

  cw_data_vector_insert (a, e, 2);

  cw_data_vector_insert (a, f, 2);

  cw_data_vector_erase (a, 3);

  cw_data_vector_set (a, 1, cw_data_duplicate_s32 (287));

  cw_data_vector_length (a, &length);

  cw_data_vector_copy (&b, a);

  cw_data_vector_compare (&equal, a, g);

  fprintf (stdout, "%d\n", equal == CW_BOOL_TRUE ? 1 : 0);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);

  cw_data_vector_destroy (&g);

  return EXIT_SUCCESS;
}
