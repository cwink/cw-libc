#include "../include/cw_error.h"

#include <stdio.h>

static char *
cw_error_level_to_string (const CW_ERROR_LEVEL level)
{
  switch (level)
    {
    case CW_ERROR_LEVEL_CRITICAL:
      return "CRITICAL";
    case CW_ERROR_LEVEL_ERROR:
      return "ERROR";
    case CW_ERROR_LEVEL_WARNING:
      return "WARNING";
    case CW_ERROR_LEVEL_MAX:
      break;
    }

  return "UNKNOWN";
}

static void
cw_error_callback_default (const CW_ERROR_LEVEL level, const char *const file,
                           const size_t line, const char *const message)
{
  if (level >= CW_ERROR_LEVEL_MAX || file == NULL || message == NULL)
    {
      return;
    }

  fprintf (level == CW_ERROR_LEVEL_WARNING ? stdout : stderr, "%s:%s:%lu:%s\n",
           cw_error_level_to_string (level), file, (unsigned long)line,
           message);

  if (level == CW_ERROR_LEVEL_CRITICAL)
    {
      exit (EXIT_FAILURE);
    }
}

static CW_ERROR_CALLBACK cw_error_callback = cw_error_callback_default;

CW_BOOL
cw_error_callback_set (const CW_ERROR_CALLBACK callback)
{
  if (callback == NULL)
    {
      return CW_BOOL_FALSE;
    }

  cw_error_callback = callback;

  return CW_BOOL_TRUE;
}

CW_ERROR_CALLBACK
cw_error_callback_get (void) { return cw_error_callback; }
