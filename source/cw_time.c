#include "../include/cw_time.h"
#include "../include/cw_error.h"

#include <stdlib.h>

#define CW_TIME_MILLISECONDS_CONVERSION 1000.0

CW_BOOL
cw_time_get_milliseconds (double *value)
{
  struct timeval tv = { 0, 0 };

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (gettimeofday (&tv, NULL) != 0)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to get time of day");

      return CW_BOOL_FALSE;
    }

  *value = (double)tv.tv_sec * CW_TIME_MILLISECONDS_CONVERSION
           + (double)tv.tv_usec / CW_TIME_MILLISECONDS_CONVERSION;

  return CW_BOOL_TRUE;
}
