#include "../include/cw_data_compare.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_error.h"
#include "../include/cw_memory.h"

#include <string.h>

#define CW_DATA_COMPARE_CREATE_PRIMATIVE(NAME, TYPE)                          \
  CW_BOOL NAME (CW_BOOL *const result, const void *const first,               \
                const void *const second)                                     \
  {                                                                           \
    const TYPE *first_ptr = NULL;                                             \
                                                                              \
    const TYPE *second_ptr = NULL;                                            \
                                                                              \
    if (result == NULL)                                                       \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");     \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (first == NULL)                                                        \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "first parameter is NULL");      \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (second == NULL)                                                       \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "second parameter is NULL");     \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    first_ptr = (const TYPE *)first;                                          \
                                                                              \
    second_ptr = (const TYPE *)second;                                        \
                                                                              \
    *result = *first_ptr == *second_ptr ? CW_BOOL_TRUE : CW_BOOL_FALSE;       \
                                                                              \
    return CW_BOOL_TRUE;                                                      \
  }

CW_BOOL
cw_data_compare_c_string (CW_BOOL *const result, const void *const first,
                          const void *const second)
{
  const char *first_ptr = NULL;

  const char *second_ptr = NULL;

  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (first == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "first parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (second == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "second parameter is NULL");

      return CW_BOOL_FALSE;
    }

  first_ptr = (const char *)first;

  second_ptr = (const char *)second;

  *result = strcmp (first_ptr, second_ptr) == 0 ? CW_BOOL_TRUE : CW_BOOL_FALSE;

  return CW_BOOL_TRUE;
}

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_char, char)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_s8, CW_S8)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_u8, CW_U8)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_s16, CW_S16)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_u16, CW_U16)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_s32, CW_S32)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_u32, CW_U32)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_s64, CW_S64)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_u64, CW_U64)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_size, size_t)

CW_DATA_COMPARE_CREATE_PRIMATIVE (cw_data_compare_bool, CW_BOOL)

CW_BOOL
cw_data_compare_vector (CW_BOOL *const result, const void *const first,
                        const void *const second)
{
  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (first == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "first parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (second == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "second parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (cw_data_vector_compare (result, (const CW_DATA_VECTOR *const)first,
                              (const CW_DATA_VECTOR *const)second)
      != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to compare vector");

      return CW_BOOL_FALSE;
    }

  return CW_BOOL_TRUE;
}
