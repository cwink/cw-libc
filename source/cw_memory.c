#include "../include/cw_memory.h"
#include "../include/cw_error.h"

CW_BOOL
cw_memory_create (void **const mem, const size_t size)
{
  void *new_mem = NULL;

  if (mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "mem parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (size == 0)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "size parameter is 0");

      return CW_BOOL_FALSE;
    }

  new_mem = malloc (size);

  if (new_mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_CRITICAL, "failed to allocate memory");

      return CW_BOOL_FALSE;
    }

  *mem = new_mem;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_memory_recreate (void **const mem, const size_t size)
{
  void *new_mem = NULL;

  if (mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "mem parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (*mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "mem parameter hasn't previously been created");

      return CW_BOOL_FALSE;
    }

  if (size == 0)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "size parameter is 0");

      return CW_BOOL_FALSE;
    }

  new_mem = realloc (*mem, size);

  if (new_mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_CRITICAL, "failed to reallocate memory");

      return CW_BOOL_FALSE;
    }

  *mem = new_mem;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_memory_destroy (void **const mem)
{
  if (mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "mem parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (*mem == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "mem parameter hasn't previously been created");

      return CW_BOOL_FALSE;
    }

  free (*mem);

  *mem = NULL;

  return CW_BOOL_TRUE;
}
