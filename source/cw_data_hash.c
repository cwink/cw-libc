#include "../include/cw_data_hash.h"
#include "../include/cw_error.h"

#define CW_DATA_HASH_FNV_OFFSET 14695981039346656037UL

#define CW_DATA_HASH_FNV_PRIME 1099511628211UL

#include <string.h>

#define CW_DATA_HASH_CREATE_PRIMATIVE(NAME, TYPE)                             \
  CW_BOOL NAME (CW_U64 *const result, const void *const data)                 \
  {                                                                           \
    if (result == NULL)                                                       \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");     \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (data == NULL)                                                         \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");     \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    *result = (CW_U64) * ((const TYPE *const)data);                           \
                                                                              \
    return CW_BOOL_TRUE;                                                      \
  }

CW_BOOL
cw_data_hash_fnv1a (CW_U64 *const result, const CW_U8 *const data,
                    const size_t length)
{
  size_t index = 0;

  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (data == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  *result = CW_DATA_HASH_FNV_OFFSET;

  for (; index < length; ++index)
    {
      *result ^= data[index];

      *result *= CW_DATA_HASH_FNV_PRIME;
    }

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_hash_jenkins (CW_U32 *const result, const CW_U8 *const data,
                      const size_t length)
{
  size_t index = 0;

  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (data == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  *result = 0;

  for (; index < length; ++index)
    {
      *result += data[index];

      *result += *result << 10U; /* NOLINT */

      *result ^= *result >> 6U; /* NOLINT */
    }

  *result += *result << 3U;

  *result ^= *result >> 11U; /* NOLINT */

  *result += *result << 15U; /* NOLINT */

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_hash_c_string (CW_U64 *const result, const void *const data)
{
  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (data == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (cw_data_hash_fnv1a (result, (const CW_U8 *const)data,
                          strlen ((const char *const)data))
      != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to generate c string hash");

      return CW_BOOL_FALSE;
    }

  return CW_BOOL_TRUE;
}

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_char, char)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_s8, CW_S8)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_u8, CW_U8)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_s16, CW_S16)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_u16, CW_U16)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_s32, CW_S32)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_u32, CW_U32)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_s64, CW_S64)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_u64, CW_U64)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_size, size_t)

CW_DATA_HASH_CREATE_PRIMATIVE (cw_data_hash_bool, CW_BOOL)
