#include "../include/cw_file.h"
#include "../include/cw_error.h"
#include "../include/cw_os.h"

#include <string.h>

CW_BOOL
cw_file_get_filename (char *const destination, const char *const source)
{
  CW_S64 index = 0;

  CW_S64 jdex = 0;

  CW_S64 slash_location = -1;

  CW_S64 dot_location = -1;

  if (destination == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "destination parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (source == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "source parameter is NULL");

      return CW_BOOL_FALSE;
    }

  for (; source[index] != '\0'; ++index)
    {
      if (source[index] == '.')
        {
          dot_location = index;

          continue;
        }

      if (source[index] == CW_OS_PATH_DELIMITER)
        {
          slash_location = index;

          continue;
        }
    }

  if (dot_location == -1 || dot_location < slash_location)
    {
      dot_location = index;
    }

  for (index = slash_location + 1; index < dot_location; ++index, ++jdex)
    {
      destination[jdex] = source[index];
    }

  destination[jdex] = '\0';

  return CW_BOOL_TRUE;
}
