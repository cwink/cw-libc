#include "../include/cw_data_destroy.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_error.h"
#include "../include/cw_memory.h"

#define CW_DATA_DESTROY_CREATE_BASIC_DESTROY(FUNCTION_NAME, TYPE, NAME)       \
  CW_BOOL FUNCTION_NAME (void **value)                                        \
  {                                                                           \
    if (value == NULL)                                                        \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");      \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (*value == NULL)                                                       \
      {                                                                       \
        CW_ERROR_CALL (                                                       \
            CW_ERROR_LEVEL_ERROR,                                             \
            "value parameters memory has not been previously created");       \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (CW_MEMORY_DESTROY (value) != CW_BOOL_TRUE)                            \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_WARNING,                                \
                       "failed to destroy " #NAME " value");                  \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    return CW_BOOL_TRUE;                                                      \
  }

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_c_string, c_string,
                                      c string)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_char, char, char)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_s8, CW_S8, CW_S8)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_u8, CW_U8, CW_U8)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_s16, CW_S16, CW_S16)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_u16, CW_U16, CW_U16)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_s32, CW_S32, CW_S32)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_u32, CW_U32, CW_U32)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_s64, CW_S64, CW_S64)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_u64, CW_U64, CW_U64)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_size, size_t, size_t)

CW_DATA_DESTROY_CREATE_BASIC_DESTROY (cw_data_destroy_bool, CW_BOOL, CW_BOOL)

CW_BOOL
cw_data_destroy_vector (void **value)
{
  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (*value == NULL)
    {
      CW_ERROR_CALL (
          CW_ERROR_LEVEL_ERROR,
          "value parameters memory has not been previously created");

      return CW_BOOL_FALSE;
    }

  if (cw_data_vector_destroy ((CW_DATA_VECTOR **)value) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to destroy vector value");

      return CW_BOOL_FALSE;
    }

  return CW_BOOL_TRUE;
}
