#include "../include/cw_data_vector.h"
#include "../include/cw_error.h"
#include "../include/cw_memory.h"

struct cw_data_vector
{
  void **data;

  size_t capacity;

  size_t length;

  CW_DATA_COPY copy;

  CW_DATA_COMPARE compare;

  CW_DATA_DESTROY destroy;
};

CW_BOOL
cw_data_vector_create (CW_DATA_VECTOR **const vector, const size_t capacity,
                       const CW_DATA_COPY copy, const CW_DATA_COMPARE compare,
                       const CW_DATA_DESTROY destroy)
{
  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (copy == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "copy parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (compare == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "compare parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (destroy == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "destroy parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (capacity < 2)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "capacity parameter should be 2 or greater");

      return CW_BOOL_FALSE;
    }

  if (capacity % 2 != 0)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "capacity parameter should be divisable by 2");

      return CW_BOOL_FALSE;
    }

  if (CW_MEMORY_CREATE (vector, CW_DATA_VECTOR, 1) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to create vector");

      return CW_BOOL_FALSE;
    }

  if (CW_MEMORY_CREATE (&(*vector)->data, void *, capacity) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to create vector data");

      CW_MEMORY_DESTROY (vector);

      return CW_BOOL_FALSE;
    }

  (*vector)->capacity = capacity;

  (*vector)->length = 0;

  (*vector)->copy = copy;

  (*vector)->compare = compare;

  (*vector)->destroy = destroy;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_copy (CW_DATA_VECTOR **const destination,
                     const CW_DATA_VECTOR *const source)
{
  CW_BOOL result = CW_BOOL_TRUE;

  if (destination == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "destination parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (source == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "source parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (cw_data_vector_create (destination, source->capacity, source->copy,
                             source->compare, source->destroy)
      != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to create vector");

      return CW_BOOL_FALSE;
    }

  if (source->data != NULL)
    {
      size_t index = 0;

      for (; index < source->length; ++index)
        {
          if (source->copy (&(*destination)->data[index], source->data[index])
              != CW_BOOL_TRUE)
            {
              CW_ERROR_CALL (CW_ERROR_LEVEL_WARNING,
                             "failed to copy all vector elements");

              result = CW_BOOL_FALSE;
            }
        }
    }

  (*destination)->length = source->length;

  return result;
}

CW_BOOL
cw_data_vector_destroy (CW_DATA_VECTOR **const vector)
{
  CW_BOOL result = CW_BOOL_TRUE;

  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if ((*vector)->data != NULL)
    {
      size_t index = 0;

      for (; index < (*vector)->length; ++index)
        {
          if ((*vector)->destroy (&(*vector)->data[index]) != CW_BOOL_TRUE)
            {
              CW_ERROR_CALL (CW_ERROR_LEVEL_WARNING,
                             "failed to destroy all vector elements");

              result = CW_BOOL_FALSE;
            }
        }

      if (CW_MEMORY_DESTROY (&(*vector)->data) != CW_BOOL_TRUE)
        {
          CW_ERROR_CALL (CW_ERROR_LEVEL_WARNING,
                         "failed to destroy vector data");

          result = CW_BOOL_FALSE;
        }
    }

  if (CW_MEMORY_DESTROY (&(*vector)) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_WARNING, "failed to destroy vector");

      return CW_BOOL_FALSE;
    }

  return result;
}

CW_BOOL
cw_data_vector_compare (CW_BOOL *result, const CW_DATA_VECTOR *const first,
                        const CW_DATA_VECTOR *const second)
{
  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (first == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "first parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (second == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "second parameter is NULL");

      return CW_BOOL_FALSE;
    }

  *result = CW_BOOL_TRUE;

  if (first->length != second->length || first->compare != second->compare)
    {
      *result = CW_BOOL_FALSE;
    }
  else
    {
      size_t index = 0;

      for (; index < first->length; ++index)
        {
          CW_BOOL res = CW_BOOL_TRUE;

          if (first->compare (&res, first->data[index], second->data[index])
              != CW_BOOL_TRUE)
            {
              CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                             "failed to compare first and second data values");

              return CW_BOOL_FALSE;
            }

          if (res != CW_BOOL_TRUE)
            {
              *result = CW_BOOL_FALSE;

              break;
            }
        }
    }

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_insert (CW_DATA_VECTOR *const vector, void *const value,
                       const size_t index)
{
  CW_S64 idx = 0;

  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (index > vector->length)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "index parameter is out of bounds");

      return CW_BOOL_FALSE;
    }

  if (vector->length + 1 > vector->capacity)
    {
      if (CW_MEMORY_RECREATE (&vector->data, void *,
                              vector->capacity * vector->capacity)
          != CW_BOOL_TRUE)
        {
          CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to resize vector data");

          return CW_BOOL_FALSE;
        }

      vector->capacity *= vector->capacity;
    }

  ++vector->length;

  for (idx = (CW_S64)vector->length - 1; idx > (CW_S64)index; --idx)
    {
      vector->data[idx] = vector->data[idx - 1];
    }

  vector->data[index] = value;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_erase (CW_DATA_VECTOR *const vector, const size_t index)
{
  CW_S64 idx = 0;

  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (vector->length == 0)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector is empty");

      return CW_BOOL_FALSE;
    }

  if (index > vector->length)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "index parameter is out of bounds");

      return CW_BOOL_FALSE;
    }

  if (vector->destroy (&vector->data[index]) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to destroy value at index");

      return CW_BOOL_FALSE;
    }

  --vector->length;

  for (idx = (CW_S64)index; idx < (CW_S64)vector->length; ++idx)
    {
      vector->data[idx] = vector->data[idx + 1];
    }

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_length (const CW_DATA_VECTOR *const vector,
                       size_t *const length)
{
  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (length == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "length parameter is NULL");

      return CW_BOOL_FALSE;
    }

  *length = vector->length;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_get (const CW_DATA_VECTOR *const vector, const size_t index,
                    void **const value)
{
  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (index >= vector->length)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "index parameter is out of bounds");

      return CW_BOOL_FALSE;
    }

  *value = vector->data[index];

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_set (const CW_DATA_VECTOR *const vector, const size_t index,
                    void *const value)
{
  if (vector == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "vector parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (index >= vector->length)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "index parameter is out of bounds");

      return CW_BOOL_FALSE;
    }

  if (vector->destroy (&vector->data[index]) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to destroy value at index");

      return CW_BOOL_FALSE;
    }

  vector->data[index] = value;

  return CW_BOOL_TRUE;
}
