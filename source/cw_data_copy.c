#include "../include/cw_data_copy.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_error.h"
#include "../include/cw_memory.h"

#include <string.h>

#define CW_DATA_COPY_CREATE_PRIMATIVE(NAME, TYPE)                             \
  CW_BOOL NAME (void **result, const void *value)                             \
  {                                                                           \
    TYPE *result_ptr = NULL; /* NOLINT */                                     \
                                                                              \
    const TYPE *value_ptr = NULL;                                             \
                                                                              \
    if (result == NULL)                                                       \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");     \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    if (value == NULL)                                                        \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");      \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    value_ptr = (const TYPE *)value;                                          \
                                                                              \
    if (CW_MEMORY_CREATE (result, TYPE, 1) != CW_BOOL_TRUE)                   \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,                                  \
                       "failed to create memory for " #TYPE " copy");         \
                                                                              \
        return CW_BOOL_FALSE;                                                 \
      }                                                                       \
                                                                              \
    result_ptr = (TYPE *)(*result);                                           \
                                                                              \
    *result_ptr = *value_ptr;                                                 \
                                                                              \
    return CW_BOOL_TRUE;                                                      \
  }

CW_BOOL
cw_data_copy_c_string (void **result, const void *const value)
{
  size_t length = 0;

  size_t index = 0;

  char *result_ptr = NULL;

  const char *value_ptr = NULL;

  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  value_ptr = (const char *)value;

  length = strlen (value_ptr);

  if (CW_MEMORY_CREATE (result, char, length + 1) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "failed to create memory for c string copy");

      return CW_BOOL_FALSE;
    }

  result_ptr = (char *)(*result);

  for (; index < length; ++index)
    {
      result_ptr[index] = value_ptr[index];
    }

  result_ptr[index] = '\0';

  return CW_BOOL_TRUE;
}

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_char, char)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_s8, CW_S8)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_u8, CW_U8)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_s16, CW_S16)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_u16, CW_U16)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_s32, CW_S32)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_u32, CW_U32)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_s64, CW_S64)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_u64, CW_U64)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_size, size_t)

CW_DATA_COPY_CREATE_PRIMATIVE (cw_data_copy_bool, CW_BOOL)

CW_BOOL
cw_data_copy_vector (void **result, const void *const value)
{
  if (result == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "result parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return CW_BOOL_FALSE;
    }

  if (cw_data_vector_copy ((CW_DATA_VECTOR **)result,
                           (const CW_DATA_VECTOR *)value)
      != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to create copy of vector");

      return CW_BOOL_FALSE;
    }

  return CW_BOOL_TRUE;
}
