#include "../include/cw_data_duplicate.h"
#include "../include/cw_error.h"
#include "../include/cw_memory.h"

#include <string.h>

#define CW_DATA_DUPLICATE_CREATE_PRIMATIVE(NAME, TYPE)                        \
  TYPE *NAME (const TYPE value)                                               \
  {                                                                           \
    TYPE *new_value = NULL; /* NOLINT */                                      \
                                                                              \
    if (CW_MEMORY_CREATE (&new_value, TYPE, 1) != CW_BOOL_TRUE)               \
      {                                                                       \
        CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,                                  \
                       "failed to allocate memory for " #TYPE "");            \
                                                                              \
        return NULL;                                                          \
      }                                                                       \
                                                                              \
    *new_value = value;                                                       \
                                                                              \
    return new_value;                                                         \
  }

char *
cw_data_duplicate_c_string (const char *const value)
{
  char *new_value = NULL;

  size_t length = 0;

  size_t index = 0;

  if (value == NULL)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "value parameter is NULL");

      return NULL;
    }

  length = strlen (value);

  if (CW_MEMORY_CREATE (&new_value, char, length + 1) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR,
                     "failed to allocate memory for c string");

      return NULL;
    }

  for (; index < length; ++index)
    {
      new_value[index] = value[index];
    }

  new_value[index] = '\0';

  return new_value;
}

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_char, char)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_s8, CW_S8)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_u8, CW_U8)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_s16, CW_S16)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_u16, CW_U16)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_s32, CW_S32)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_u32, CW_U32)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_s64, CW_S64)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_u64, CW_U64)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_size, size_t)

CW_DATA_DUPLICATE_CREATE_PRIMATIVE (cw_data_duplicate_bool, CW_BOOL)

CW_DATA_VECTOR *
cw_data_duplicate_vector (const CW_DATA_VECTOR *const value)
{
  CW_DATA_VECTOR *new_value = NULL;

  if (cw_data_vector_copy (&new_value, value) != CW_BOOL_TRUE)
    {
      CW_ERROR_CALL (CW_ERROR_LEVEL_ERROR, "failed to duplicate vector");

      return NULL;
    }

  return new_value;
}

