#include "../include/cw_error.h"
#include "../include/cw_test.h"

CW_TEST_CREATE (error, callback_set, {
  CW_TEST_ASSERT (cw_error_callback_set (NULL) == CW_BOOL_FALSE);
})

CW_TEST_CREATE (error, callback_get,
                { CW_TEST_ASSERT (cw_error_callback_get () != NULL); })

CW_TEST_MAIN ({
  CW_TEST_CALL (error, callback_set);

  CW_TEST_CALL (error, callback_get);
})
