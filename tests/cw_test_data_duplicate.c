#include "../include/cw_data_compare.h"
#include "../include/cw_data_copy.h"
#include "../include/cw_data_destroy.h"
#include "../include/cw_data_duplicate.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_memory.h"
#include "../include/cw_test.h"

#include <string.h>

#define CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST(                         \
    TYPE, FIRST_VALUE, SECOND_VALUE, FUNCTION_NAME)                           \
  CW_TEST_CREATE (data_duplicate, TYPE, {                                     \
    const TYPE a = FIRST_VALUE;                                               \
                                                                              \
    const TYPE b = SECOND_VALUE;                                              \
                                                                              \
    TYPE *c = NULL; /* NOLINT */                                              \
                                                                              \
    c = FUNCTION_NAME (a);                                                    \
                                                                              \
    CW_TEST_ASSERT (*c == a);                                                 \
                                                                              \
    CW_MEMORY_DESTROY (&c);                                                   \
                                                                              \
    c = FUNCTION_NAME (b);                                                    \
                                                                              \
    CW_TEST_ASSERT (*c == b);                                                 \
                                                                              \
    CW_MEMORY_DESTROY (&c);                                                   \
  })

CW_TEST_CREATE (data_duplicate, c_string, {
  const char *const a = "This is a test for a duplicate.";

  char *b = NULL;

  const char *const c = "";

  CW_TEST_ASSERT (cw_data_duplicate_c_string (NULL) == NULL);

  b = cw_data_duplicate_c_string (a);

  CW_TEST_ASSERT (strcmp (a, b) == 0);

  CW_MEMORY_DESTROY (&b);

  b = cw_data_duplicate_c_string (c);

  CW_TEST_ASSERT (strcmp (c, b) == 0);

  CW_MEMORY_DESTROY (&b);
})

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (char, 'T', '\0',
                                              cw_data_duplicate_char)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_S8, 16, -15,
                                              cw_data_duplicate_s8)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_U8, 45, 84,
                                              cw_data_duplicate_u8)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_S16, 104, -243,
                                              cw_data_duplicate_s16)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_U16, 103, 200,
                                              cw_data_duplicate_u16)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_S32, 2984, -8273,
                                              cw_data_duplicate_s32)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_U32, 3098, 4489,
                                              cw_data_duplicate_u32)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_S64, 100098, -298323,
                                              cw_data_duplicate_s64)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_U64, 989832, 444787,
                                              cw_data_duplicate_u64)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (size_t, 1092324, 898389,
                                              cw_data_duplicate_size)

CW_TEST_DATA_DUPLICATE_CREATE_PRIMATIVE_TEST (CW_BOOL, CW_BOOL_TRUE,
                                              CW_BOOL_FALSE,
                                              cw_data_duplicate_bool)

CW_TEST_CREATE (data_duplicate, vector, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_BOOL result = CW_BOOL_FALSE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (75), 2);

  b = cw_data_duplicate_vector (NULL);

  CW_TEST_ASSERT (b == NULL);

  b = cw_data_duplicate_vector (a);

  CW_TEST_ASSERT (b != NULL);

  cw_data_compare_vector (&result, a, b);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (data_duplicate, c_string);

  CW_TEST_CALL (data_duplicate, char);

  CW_TEST_CALL (data_duplicate, CW_S8);

  CW_TEST_CALL (data_duplicate, CW_U8);

  CW_TEST_CALL (data_duplicate, CW_S16);

  CW_TEST_CALL (data_duplicate, CW_U16);

  CW_TEST_CALL (data_duplicate, CW_S32);

  CW_TEST_CALL (data_duplicate, CW_U32);

  CW_TEST_CALL (data_duplicate, CW_S64);

  CW_TEST_CALL (data_duplicate, CW_U64);

  CW_TEST_CALL (data_duplicate, size_t);

  CW_TEST_CALL (data_duplicate, CW_BOOL);

  CW_TEST_CALL (data_duplicate, vector);
})
