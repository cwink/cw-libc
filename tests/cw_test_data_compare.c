#include "../include/cw_data_compare.h"
#include "../include/cw_data_copy.h"
#include "../include/cw_data_destroy.h"
#include "../include/cw_data_duplicate.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_test.h"

#define CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST(                           \
    TYPE, INITIAL_VALUE_FIRST, INITIAL_VALUE_SECOND, INITIAL_VALUE_THIRD,     \
    FUNCTION_NAME)                                                            \
  CW_TEST_CREATE (data_compare, TYPE, {                                       \
    TYPE first = INITIAL_VALUE_FIRST;                                         \
                                                                              \
    TYPE second = INITIAL_VALUE_SECOND;                                       \
                                                                              \
    TYPE third = INITIAL_VALUE_THIRD;                                         \
                                                                              \
    CW_BOOL result = CW_BOOL_FALSE;                                           \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&result, NULL, &second) == CW_BOOL_FALSE); \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&result, &first, NULL) == CW_BOOL_FALSE);  \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (NULL, &first, &second) == CW_BOOL_FALSE);  \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&result, &first, &second)                  \
                    == CW_BOOL_TRUE);                                         \
                                                                              \
    CW_TEST_ASSERT (result == CW_BOOL_TRUE);                                  \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&result, &first, &third) == CW_BOOL_TRUE); \
                                                                              \
    CW_TEST_ASSERT (result == CW_BOOL_FALSE);                                 \
  })

CW_TEST_CREATE (data_compare, c_string, {
  const char *const first = "This is a test.";

  const char *const second = "This is a test.";

  const char *const third = "This is not a test.";

  const char *const fourth = "";

  const char *const fifth = "";

  CW_BOOL result = CW_BOOL_FALSE;

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, NULL, &second)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, &first, NULL)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_c_string (NULL, &first, &second)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, &first, &second)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, &first, &third)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, &fourth, &fifth)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_compare_c_string (&result, &fourth, &third)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);
})

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (char, 'A', 'A', 'Z',
                                            cw_data_compare_char)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_S8, -32, -32, -40,
                                            cw_data_compare_s8)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_U8, 32, 32, 40,
                                            cw_data_compare_u8)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_S16, -400, -400, -313,
                                            cw_data_compare_s16)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_U16, 400, 400, 313,
                                            cw_data_compare_u16)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_S32, -2400, -2400, -2987,
                                            cw_data_compare_s32)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_U32, 2400, 2400, 2987,
                                            cw_data_compare_u32)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_S64, -32000, -32000, -87687,
                                            cw_data_compare_s64)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_U64, 32000, 32000, 87687,
                                            cw_data_compare_u64)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (size_t, 640000, 640000, 87000,
                                            cw_data_compare_size)

CW_TEST_DATA_COMPARE_CREATE_PRIMATIVE_TEST (CW_BOOL, CW_BOOL_TRUE,
                                            CW_BOOL_TRUE, CW_BOOL_FALSE,
                                            cw_data_compare_bool)

CW_TEST_CREATE (data_compare, vector, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_DATA_VECTOR *c = NULL;

  CW_DATA_VECTOR *d = NULL;

  CW_DATA_VECTOR *e = NULL;

  CW_BOOL result = CW_BOOL_TRUE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&b, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&c, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&d, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&e, char, 2, cw_data_copy_char, cw_data_compare_char,
                         cw_data_destroy_char);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (c, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (c, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (88), 0);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (e, cw_data_duplicate_char ('A'), 0);

  cw_data_vector_insert (e, cw_data_duplicate_char ('B'), 1);

  cw_data_vector_insert (e, cw_data_duplicate_char ('C'), 2);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, NULL, b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, a, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_vector (NULL, a, b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, a, b) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, a, c) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, a, d) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_compare_vector (&result, a, e) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  cw_data_vector_destroy (&e);

  cw_data_vector_destroy (&d);

  cw_data_vector_destroy (&c);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (data_compare, c_string);

  CW_TEST_CALL (data_compare, char);

  CW_TEST_CALL (data_compare, CW_S8);

  CW_TEST_CALL (data_compare, CW_U8);

  CW_TEST_CALL (data_compare, CW_S16);

  CW_TEST_CALL (data_compare, CW_U16);

  CW_TEST_CALL (data_compare, CW_S32);

  CW_TEST_CALL (data_compare, CW_U32);

  CW_TEST_CALL (data_compare, CW_S64);

  CW_TEST_CALL (data_compare, CW_U64);

  CW_TEST_CALL (data_compare, size_t);

  CW_TEST_CALL (data_compare, CW_BOOL);

  CW_TEST_CALL (data_compare, vector);
})
