#include "../include/cw_data_compare.h"
#include "../include/cw_data_copy.h"
#include "../include/cw_data_destroy.h"
#include "../include/cw_data_duplicate.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_memory.h"
#include "../include/cw_test.h"

#include <string.h>

#define CW_TEST_DATA_CREATE_PRIMATIVE_TEST(                                   \
    TYPE, INITIAL_VALUE, COPY_FUNCTION_NAME, DESTROY_FUNCTION_NAME)           \
  CW_TEST_CREATE (data_destroy, TYPE, {                                       \
    TYPE *a = NULL; /* NOLINT */                                              \
                                                                              \
    TYPE b = INITIAL_VALUE;                                                   \
                                                                              \
    COPY_FUNCTION_NAME ((void **)&a, &b);                                     \
                                                                              \
    CW_TEST_ASSERT (DESTROY_FUNCTION_NAME ((void **)&a) == CW_BOOL_TRUE);     \
                                                                              \
    CW_TEST_ASSERT (a == NULL);                                               \
                                                                              \
    CW_TEST_ASSERT (DESTROY_FUNCTION_NAME (NULL) == CW_BOOL_FALSE);           \
  })

CW_TEST_CREATE (data_destroy, c_string, {
  char *a = NULL;

  char *b = "This is a test 123.";

  cw_data_copy_c_string ((void **)&a, &b);

  CW_TEST_ASSERT (cw_data_destroy_c_string ((void **)&a) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (a == NULL);

  CW_TEST_ASSERT (cw_data_destroy_c_string (NULL) == CW_BOOL_FALSE);
})

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (char, 'A', cw_data_copy_char,
                                    cw_data_destroy_char)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_S8, -32, cw_data_copy_s8,
                                    cw_data_destroy_s8)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_U8, 32, cw_data_copy_u8,
                                    cw_data_destroy_u8)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_S16, -300, cw_data_copy_s16,
                                    cw_data_destroy_s16)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_U16, 300, cw_data_copy_u16,
                                    cw_data_destroy_u16)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_S32, -2000, cw_data_copy_s32,
                                    cw_data_destroy_s32)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_U32, 2000, cw_data_copy_u32,
                                    cw_data_destroy_u32)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_S64, -30000, cw_data_copy_s64,
                                    cw_data_destroy_s64)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_U64, 30000, cw_data_copy_u64,
                                    cw_data_destroy_u64)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (size_t, 300000, cw_data_copy_size,
                                    cw_data_destroy_size)

CW_TEST_DATA_CREATE_PRIMATIVE_TEST (CW_BOOL, CW_BOOL_TRUE, cw_data_copy_bool,
                                    cw_data_destroy_bool)

CW_TEST_CREATE (data_destroy, vector, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (75), 2);

  CW_TEST_ASSERT (cw_data_destroy_vector ((void **)&a) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (a == NULL);

  CW_TEST_ASSERT (cw_data_destroy_vector (NULL) == CW_BOOL_FALSE);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (data_destroy, c_string);

  CW_TEST_CALL (data_destroy, char);

  CW_TEST_CALL (data_destroy, CW_S8);

  CW_TEST_CALL (data_destroy, CW_U8);

  CW_TEST_CALL (data_destroy, CW_S16);

  CW_TEST_CALL (data_destroy, CW_U16);

  CW_TEST_CALL (data_destroy, CW_S32);

  CW_TEST_CALL (data_destroy, CW_U32);

  CW_TEST_CALL (data_destroy, CW_S64);

  CW_TEST_CALL (data_destroy, CW_U64);

  CW_TEST_CALL (data_destroy, size_t);

  CW_TEST_CALL (data_destroy, CW_BOOL);

  CW_TEST_CALL (data_destroy, vector);
})
