#include "../include/cw_data_hash.h"
#include "../include/cw_test.h"

#include <string.h>

#define CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST(                              \
    TYPE, INITIAL_VALUE_FIRST, INITIAL_VALUE_SECOND, EXPECTED_VALUE_FIRST,    \
    EXPECTED_VALUE_SECOND, FUNCTION_NAME)                                     \
  CW_TEST_CREATE (data_hash, TYPE, {                                          \
    CW_U64 hash = 0;                                                          \
                                                                              \
    TYPE a = INITIAL_VALUE_FIRST;                                             \
                                                                              \
    TYPE b = INITIAL_VALUE_SECOND;                                            \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (NULL, NULL) == CW_BOOL_FALSE);             \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (NULL, &a) == CW_BOOL_FALSE);               \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&hash, NULL) == CW_BOOL_FALSE);            \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&hash, &a) == CW_BOOL_TRUE);               \
                                                                              \
    CW_TEST_ASSERT (hash == (EXPECTED_VALUE_FIRST));                          \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (&hash, &b) == CW_BOOL_TRUE);               \
                                                                              \
    CW_TEST_ASSERT (hash == (EXPECTED_VALUE_SECOND));                         \
  })

CW_TEST_CREATE (data_hash, fnv1a, {
  CW_U64 hash = 0;

  const char *a = "This is a test.";

  const char *b = "T";

  const char *c = "";

  CW_TEST_ASSERT (cw_data_hash_fnv1a (NULL, NULL, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_fnv1a (NULL, (const CW_U8 *)a, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_fnv1a (&hash, NULL, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_fnv1a (&hash, (const CW_U8 *)a, strlen (a))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 236522647116284426UL);

  CW_TEST_ASSERT (cw_data_hash_fnv1a (&hash, (const CW_U8 *)b, strlen (b))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 12638236678578911491UL);

  CW_TEST_ASSERT (cw_data_hash_fnv1a (&hash, (const CW_U8 *)c, strlen (c))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 14695981039346656037UL);
})

CW_TEST_CREATE (data_hash, jenkins, {
  CW_U32 hash = 0;

  const char *a = "This is a test.";

  const char *b = "T";

  const char *c = "";

  CW_TEST_ASSERT (cw_data_hash_jenkins (NULL, NULL, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_jenkins (NULL, (const CW_U8 *)a, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_jenkins (&hash, NULL, strlen (a))
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_jenkins (&hash, (const CW_U8 *)a, strlen (a))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 3498212464U);

  CW_TEST_ASSERT (cw_data_hash_jenkins (&hash, (const CW_U8 *)b, strlen (b))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 4268555458U);

  CW_TEST_ASSERT (cw_data_hash_jenkins (&hash, (const CW_U8 *)c, strlen (c))
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 0U);
})

CW_TEST_CREATE (data_hash, c_string, {
  CW_U64 hash = 0;

  const char *a = "This is a test.";

  const char *b = "T";

  const char *c = "";

  CW_TEST_ASSERT (cw_data_hash_c_string (NULL, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_c_string (NULL, a) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_c_string (&hash, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_hash_c_string (&hash, a) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 236522647116284426UL);

  CW_TEST_ASSERT (cw_data_hash_c_string (&hash, b) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 12638236678578911491UL);

  CW_TEST_ASSERT (cw_data_hash_c_string (&hash, c) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (hash == 14695981039346656037UL);
})

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (char, 'A', '\0', 65, 0,
                                         cw_data_hash_char)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_S8, -16, 18,
                                         18446744073709551600UL, 18UL,
                                         cw_data_hash_s8)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_U8, 16, 32, 16UL, 32UL,
                                         cw_data_hash_u8)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_S16, -103, 104,
                                         18446744073709551513UL, 104UL,
                                         cw_data_hash_s16)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_U16, 201, 103, 201UL, 103UL,
                                         cw_data_hash_u16)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_S32, -1098, 9878,
                                         18446744073709550518UL, 9878UL,
                                         cw_data_hash_s32)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_U32, 7878, 6766, 7878UL, 6766UL,
                                         cw_data_hash_u32)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_S64, -9988728, 776687,
                                         18446744073699562888UL, 776687UL,
                                         cw_data_hash_s64)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_U64, 8967421, 7362867, 8967421UL,
                                         7362867UL, cw_data_hash_u64)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (size_t, 898888822, 188227742,
                                         898888822UL, 188227742UL,
                                         cw_data_hash_size)

CW_TEST_DATA_HASH_CREATE_PRIMATIVE_TEST (CW_BOOL, 1, 0, CW_BOOL_TRUE,
                                         CW_BOOL_FALSE, cw_data_hash_bool)

CW_TEST_MAIN ({
  CW_TEST_CALL (data_hash, fnv1a);

  CW_TEST_CALL (data_hash, jenkins);

  CW_TEST_CALL (data_hash, c_string);

  CW_TEST_CALL (data_hash, char);

  CW_TEST_CALL (data_hash, CW_S8);

  CW_TEST_CALL (data_hash, CW_U8);

  CW_TEST_CALL (data_hash, CW_S16);

  CW_TEST_CALL (data_hash, CW_U16);

  CW_TEST_CALL (data_hash, CW_S32);

  CW_TEST_CALL (data_hash, CW_U32);

  CW_TEST_CALL (data_hash, CW_S64);

  CW_TEST_CALL (data_hash, CW_U64);

  CW_TEST_CALL (data_hash, size_t);

  CW_TEST_CALL (data_hash, CW_BOOL);
})
