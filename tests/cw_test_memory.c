#include "../include/cw_memory.h"
#include "../include/cw_test.h"

CW_TEST_CREATE (memory, create, {
  int *a = NULL;

  CW_TEST_ASSERT (CW_MEMORY_CREATE (&a, int, 3) == CW_BOOL_TRUE);

  a[0] = 1;

  a[1] = 2;

  a[2] = 3;

  CW_TEST_ASSERT (a[0] == 1);

  CW_TEST_ASSERT (a[1] == 2);

  CW_TEST_ASSERT (a[2] == 3);

  CW_MEMORY_DESTROY (&a);

  CW_TEST_ASSERT (CW_MEMORY_CREATE (&a, int, 0) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_MEMORY_CREATE (NULL, int, 0) == CW_BOOL_FALSE);
})

CW_TEST_CREATE (memory, recreate, {
  int *a = NULL;

  int *b = NULL;

  CW_MEMORY_CREATE (&a, int, 3);

  CW_TEST_ASSERT (CW_MEMORY_RECREATE (&a, int, 8) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (CW_MEMORY_RECREATE (&a, int, 4) == CW_BOOL_TRUE);

  CW_MEMORY_DESTROY (&a);

  CW_TEST_ASSERT (CW_MEMORY_RECREATE (&b, int, 4) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_MEMORY_RECREATE (NULL, int, 4) == CW_BOOL_FALSE);
})

CW_TEST_CREATE (memory, destroy, {
  int *a = NULL;

  int *b = NULL;

  CW_MEMORY_CREATE (&a, int, 3);

  CW_TEST_ASSERT (CW_MEMORY_DESTROY (&a) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (CW_MEMORY_DESTROY (&b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_MEMORY_DESTROY (NULL) == CW_BOOL_FALSE);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (memory, create);

  CW_TEST_CALL (memory, recreate);

  CW_TEST_CALL (memory, destroy);
})
