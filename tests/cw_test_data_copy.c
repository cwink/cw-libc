#include "../include/cw_data_compare.h"
#include "../include/cw_data_copy.h"
#include "../include/cw_data_destroy.h"
#include "../include/cw_data_duplicate.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_memory.h"
#include "../include/cw_test.h"

#include <string.h>

#define CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST(TYPE, INITIAL_VALUE,          \
                                                FUNCTION_NAME)                \
  CW_TEST_CREATE (data_copy, TYPE, {                                          \
    TYPE *a = NULL; /* NOLINT */                                              \
                                                                              \
    TYPE b = INITIAL_VALUE;                                                   \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME (NULL, &b) == CW_BOOL_FALSE);               \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME ((void **)&a, NULL) == CW_BOOL_FALSE);      \
                                                                              \
    CW_TEST_ASSERT (FUNCTION_NAME ((void **)&a, &b) == CW_BOOL_TRUE);         \
                                                                              \
    CW_TEST_ASSERT (*a == b);                                                 \
                                                                              \
    CW_MEMORY_DESTROY (&a);                                                   \
  })

CW_TEST_CREATE (data_copy, c_string, {
  char *a = NULL;

  char *b = "This is a test...";

  char *c = "";

  CW_TEST_ASSERT (cw_data_copy_c_string (NULL, b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_copy_c_string ((void **)&a, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_copy_c_string ((void **)&a, b) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (a, b) == 0);

  CW_MEMORY_DESTROY (&a);

  CW_TEST_ASSERT (cw_data_copy_c_string ((void **)&a, c) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (a, c) == 0);

  CW_MEMORY_DESTROY (&a);
})

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (char, 'A', cw_data_copy_char)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_S8, -32, cw_data_copy_s8)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_U8, 32, cw_data_copy_u8)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_S16, -400, cw_data_copy_s16)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_U16, 400, cw_data_copy_u16)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_S32, -2400, cw_data_copy_s32)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_U32, 2400, cw_data_copy_u32)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_S64, -32000, cw_data_copy_s64)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_U64, 32000, cw_data_copy_u64)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (size_t, 640000, cw_data_copy_size)

CW_TEST_DATA_COPY_CREATE_PRIMATIVE_TEST (CW_BOOL, CW_BOOL_TRUE,
                                         cw_data_copy_bool)

CW_TEST_CREATE (data_copy, vector, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_BOOL result = CW_BOOL_FALSE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (75), 2);

  CW_TEST_ASSERT (cw_data_copy_vector (NULL, a) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_copy_vector ((void **)&b, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_copy_vector ((void **)&b, a) == CW_BOOL_TRUE);

  cw_data_compare_vector (&result, a, b);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (data_copy, c_string);

  CW_TEST_CALL (data_copy, char);

  CW_TEST_CALL (data_copy, CW_S8);

  CW_TEST_CALL (data_copy, CW_U8);

  CW_TEST_CALL (data_copy, CW_S16);

  CW_TEST_CALL (data_copy, CW_U16);

  CW_TEST_CALL (data_copy, CW_S32);

  CW_TEST_CALL (data_copy, CW_U32);

  CW_TEST_CALL (data_copy, CW_S64);

  CW_TEST_CALL (data_copy, CW_U64);

  CW_TEST_CALL (data_copy, size_t);

  CW_TEST_CALL (data_copy, CW_BOOL);

  CW_TEST_CALL (data_copy, vector);
})
