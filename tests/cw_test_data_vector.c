#include "../include/cw_data_duplicate.h"
#include "../include/cw_data_vector.h"
#include "../include/cw_test.h"

CW_TEST_CREATE (data_vector, create, {
  CW_DATA_VECTOR *a = NULL;

  CW_TEST_ASSERT (CW_DATA_VECTOR_CREATE (NULL, CW_S32, 2, cw_data_copy_s32,
                                         cw_data_compare_s32,
                                         cw_data_destroy_s32)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, NULL,
                                         cw_data_compare_s32,
                                         cw_data_destroy_s32)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, NULL,
                                         cw_data_destroy_s32)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32,
                                         cw_data_compare_s32, NULL)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32,
                                         cw_data_compare_s32,
                                         cw_data_destroy_s32)
                  == CW_BOOL_TRUE);

  cw_data_vector_destroy (&a);
})

CW_TEST_CREATE (data_vector, destroy, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_TEST_ASSERT (cw_data_vector_destroy (NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_destroy (&a) == CW_BOOL_TRUE);
})

CW_TEST_CREATE (data_vector, copy, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_BOOL result = CW_BOOL_FALSE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-88), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (79), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-101), 2);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (989), 3);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-1), 4);

  CW_TEST_ASSERT (cw_data_vector_copy (NULL, a) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_copy (&b, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_copy (&b, a) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (b != NULL);

  cw_data_vector_compare (&result, a, b);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  cw_data_vector_destroy (&a);

  cw_data_vector_destroy (&b);
})

CW_TEST_CREATE (data_vector, compare, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_DATA_VECTOR *c = NULL;

  CW_DATA_VECTOR *d = NULL;

  CW_DATA_VECTOR *e = NULL;

  CW_BOOL result = CW_BOOL_TRUE;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&b, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&c, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&d, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&e, char, 2, cw_data_copy_char, cw_data_compare_char,
                         cw_data_destroy_char);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (c, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (c, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (88), 0);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (d, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (e, cw_data_duplicate_char ('A'), 0);

  cw_data_vector_insert (e, cw_data_duplicate_char ('B'), 1);

  cw_data_vector_insert (e, cw_data_duplicate_char ('C'), 2);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, NULL, b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, a, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_compare (NULL, a, b) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, a, b) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, a, c) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, a, d) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_compare (&result, a, e) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (result == CW_BOOL_FALSE);

  cw_data_vector_destroy (&e);

  cw_data_vector_destroy (&d);

  cw_data_vector_destroy (&c);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_CREATE (data_vector, insert, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_S32 *ch = NULL;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&b, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_TEST_ASSERT (cw_data_vector_insert (a, cw_data_duplicate_s32 (1001), 1)
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_insert (a, cw_data_duplicate_s32 (1001), 0)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_insert (b, cw_data_duplicate_s32 (-12), 0)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_insert (b, cw_data_duplicate_s32 (100), 1)
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_insert (b, cw_data_duplicate_s32 (75), 2)
                  == CW_BOOL_TRUE);

  cw_data_vector_get (a, 0, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == 1001);

  cw_data_vector_get (b, 0, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == -12);

  cw_data_vector_get (b, 1, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == 100);

  cw_data_vector_get (b, 2, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == 75);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_CREATE (data_vector, erase, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_S32 *ch = NULL;

  size_t length = 0;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&b, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (1001), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-2), 3);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (87), 4);

  CW_TEST_ASSERT (cw_data_vector_erase (NULL, 0) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_erase (a, 1) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_erase (a, 0) == CW_BOOL_TRUE);

  cw_data_vector_length (a, &length);

  CW_TEST_ASSERT (length == 0);

  CW_TEST_ASSERT (cw_data_vector_erase (b, 0) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_erase (b, 3) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (cw_data_vector_erase (b, 1) == CW_BOOL_TRUE);

  cw_data_vector_get (b, 0, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == 100);

  cw_data_vector_get (b, 1, (void **)&ch);

  CW_TEST_ASSERT (ch != NULL && *ch == -2);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_CREATE (data_vector, length, {
  CW_DATA_VECTOR *a = NULL;

  CW_DATA_VECTOR *b = NULL;

  CW_S32 *ch = NULL;

  size_t length = 0;

  CW_DATA_VECTOR_CREATE (&a, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  CW_DATA_VECTOR_CREATE (&b, CW_S32, 2, cw_data_copy_s32, cw_data_compare_s32,
                         cw_data_destroy_s32);

  cw_data_vector_insert (a, cw_data_duplicate_s32 (1001), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-12), 0);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (100), 1);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (75), 2);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (-2), 3);

  cw_data_vector_insert (b, cw_data_duplicate_s32 (87), 4);

  CW_TEST_ASSERT (cw_data_vector_length (a, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_length (NULL, &length) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_data_vector_length (a, &length) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (length == 1);

  cw_data_vector_erase (a, 0);

  CW_TEST_ASSERT (cw_data_vector_length (a, &length) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (length == 0);

  CW_TEST_ASSERT (cw_data_vector_length (b, &length) == CW_BOOL_TRUE);

  CW_TEST_ASSERT (length == 5);

  cw_data_vector_destroy (&b);

  cw_data_vector_destroy (&a);
})

CW_TEST_MAIN ({
  CW_TEST_CALL (data_vector, create);

  CW_TEST_CALL (data_vector, destroy);

  CW_TEST_CALL (data_vector, copy);

  CW_TEST_CALL (data_vector, compare);

  CW_TEST_CALL (data_vector, insert);

  CW_TEST_CALL (data_vector, erase);

  CW_TEST_CALL (data_vector, length);
})
