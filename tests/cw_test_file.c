#include "../include/cw_file.h"
#include "../include/cw_test.h"

#include <string.h>

CW_TEST_CREATE (file, get_filename, {
  char buffer[64];

  CW_TEST_ASSERT (cw_file_get_filename (NULL, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, NULL) == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_file_get_filename (NULL, "/some/path/to_this.c")
                  == CW_BOOL_FALSE);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "/some/path/to_this.c")
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "/some/path/to_this.c.x")
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this.c") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "/some/path/to_.x.this.c")
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_.x.this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "/to_this.c") == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "/to_this") == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "to_this") == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "to_this.x") == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "y.to_this.x")
                  == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "y.to_this") == 0);

  CW_TEST_ASSERT (cw_file_get_filename (buffer, "") == CW_BOOL_TRUE);

  CW_TEST_ASSERT (strcmp (buffer, "") == 0);
})

CW_TEST_MAIN ({ CW_TEST_CALL (file, get_filename); })
